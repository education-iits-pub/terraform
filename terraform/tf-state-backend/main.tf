provider "google" {
  project = "iits-consulting"
  region  = "us-central1"
  zone    = "us-central1-c"
}

resource "google_storage_bucket" "tf-state-backend-victor" {
  name          = "tf-state-backend-victor"
  location      = "EU"
  force_destroy = true
  bucket_policy_only = true
}

resource "google_storage_bucket" "tf-state-backend-prod" {
  name          = "tf-state-backend-prod"
  location      = "EU"
  force_destroy = true
  bucket_policy_only = true
}