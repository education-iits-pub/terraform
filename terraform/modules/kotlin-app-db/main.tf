variable "db_name" {
  type = string
}
variable "db_instance_name" {
  type = string
}
variable "kotlin_app_db_password" {
  type = string
}

resource "google_sql_database_instance" "kotlin_app_db_instance" {
  name = var.db_instance_name
  database_version = "POSTGRES_11"
  region = "us-central1"
  settings {
    ip_configuration {
      ipv4_enabled = true
      require_ssl = false
      authorized_networks {
        name = "public"
        value = "0.0.0.0/0"
      }
    }
    tier = "db-g1-small"
  }
}

resource "google_sql_database" "kotlin_app_db" {
  name = var.db_name
  instance = google_sql_database_instance.kotlin_app_db_instance.name
}

resource "google_sql_user" "kotlin_app" {
  name = "kotlin-app"
  instance = google_sql_database_instance.kotlin_app_db_instance.name
  password = var.kotlin_app_db_password
}

output "db_public_ip" {
  value = google_sql_database_instance.kotlin_app_db_instance.public_ip_address
}