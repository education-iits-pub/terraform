terraform {
  backend "gcs" {
    bucket = "tf-state-backend-victor"
  }
}

provider "google" {
  project = "iits-consulting"
  region  = "us-central1"
  zone    = "us-central1-c"
}

module "k8s-cluster" {
  source = "../modules/staging-k8s-cluster"
  cluster_name = "victor"
  master_auth_username = var.master_auth_username
  master_auth_password = var.master_auth_password
}

module "kotlin-app-db" {
  source = "../modules/kotlin-app-db"
  db_name = "kotlin-app"
  db_instance_name = "kotlin-app2"
  kotlin_app_db_password = var.kotlin_app_db_password
}

output "db_public_ip" {
  value = module.kotlin-app-db.db_public_ip
}